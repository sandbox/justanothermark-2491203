<?php
/**
 * @file
 * Contains ListerMenuChildrenPlugin class.
 */

/**
 * Class ListerMenuChildrenPlugin.
 *
 * Provides a Lister Plugin based around EntityFieldQuery for user configured
 * content listing.
 *
 * @see ListerPlugin
 * @see EntityFieldQuery
 */
class ListerMenuChildrenPlugin extends ListerPlugin {
  /**
   * {@inheritdoc}
   */
  public function render(array $item) {
    // Fetch the child menu links of the current path.
    $menu_children = $this->getMenuChildren(current_path());

    // Search the child menu links for nodes that we can render.
    $nodes = array();
    foreach ($menu_children as $child_link) {
      if ($node = menu_get_object('node', 1, $child_link['link']['link_path'])) {
        $nodes[$node->nid] = $node;
      }
    }

    // Render any nodes found in the menu links.
    $output = !empty($nodes) ? node_view_multiple($nodes) : array();
    return $output;
  }

  /**
   * Find the menu link children of a path.
   *
   * @param string $path
   *   The path to find the children of.
   *
   * @return mixed
   *   The fully built menu tree of
   */
  private function getMenuChildren($path) {
    $parent = menu_link_get_preferred($path);
    $parameters = array(
      'active_trail' => array($parent['plid']),
      'only_active_trail' => FALSE,
      'min_depth' => $parent['depth'] + 1,
      'max_depth' => $parent['depth'] + 1,
      'conditions' => array('plid' => $parent['mlid']),
    );

    return menu_build_tree($parent['menu_name'], $parameters);
  }

}
