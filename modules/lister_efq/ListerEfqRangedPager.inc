<?php
/**
 * @file
 * Contains the ListerEfqRangedPager class.
 */

/**
 * Class ListerEfqRangedPager.
 *
 * Extends EntityFieldQuery to prevent range being overridden by pager.
 *
 * @see EntityFieldQuery
 */
class ListerEfqRangedPager extends EntityFieldQuery {

  /**
   * {@inheritdoc}
   */
  public function initializePager() {
    if ($this->pager && !empty($this->pager['limit']) && !$this->count) {
      $page = pager_find_page($this->pager['element']);
      $count_query = clone $this;
      $this->pager['total'] = $count_query->count()->execute();
      $this->pager['start'] = $page * $this->pager['limit'];
      pager_default_initialize($this->pager['total'], $this->pager['limit'], $this->pager['element']);

      // Total limit calculated from the pager total (which used the count query
      // with the original range).
      $total_limit = $this->pager['total'] - $this->pager['start'];
      $this->range($this->pager['start'], min($total_limit, $this->pager['limit']));
    }
  }

}
