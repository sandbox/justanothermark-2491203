<?php
/**
 * @file
 * Contains ListerEfqPlugin class.
 */

/**
 * Class ListerEfqPlugin.
 *
 * Provides a Lister Plugin based around EntityFieldQuery for user configured
 * content listing.
 *
 * @see ListerPlugin
 * @see EntityFieldQuery
 */
class ListerEfqPlugin extends ListerPlugin {
  /**
   * {@inheritdoc}
   */
  public function form(array $item, $delta, $element, $instance) {
    $form = array();

    // Add defaults to the item data.
    $content_type_keys = array_keys(node_type_get_names());
    $item['data'] += array(
      'group' => '',
      'content_type' => reset($content_type_keys),
      'term_any' => array('vocab' => '', 'terms' => array()),
      'term_all' => array('vocab' => '', 'terms' => array()),
      'term_none' => array('vocab' => '', 'terms' => array()),
      'order' => array('order_by' => '', 'order_method' => ''),
      'age_limit' => '',
      'item_limit' => 0,
      'item_pager' => 10,
      'display_mode' => '',
    );

    // Add a field to select the content type this will be a list of.
    $form['content_type'] = array(
      '#type' => 'select',
      '#title' => t('Content Type'),
      '#description' => t('Select the content type to include in this list.'),
      '#options' => node_type_get_names(),
      '#default_value' => $item['data']['content_type'],
      '#empty_option' => ' - ' . t('None') . ' - ',
    );

    if (module_exists('og')) {
      $groups = node_load_multiple(og_get_all_group());
      $group_options = array('' => ' - ' . t('All') . ' - ');
      $group_options = $group_options + array_map(function ($item) {
        return $item->title;
      }, $groups);
      // Content in group option.
      $form['group'] = array(
        '#type' => 'select',
        '#title' => t('Group'),
        '#description' => t('Only content in this group will be shown in this list.'),
        '#options' => $group_options,
        '#default_value' => $item['data']['group'],
      );
    }

    if (module_exists('taxonomy')) {
      // Load vocabulary options for all term fields.
      $vocabulary_options = $this->getVocabularyOptions();
      $term_options = array();

      foreach ($vocabulary_options as $vocab_machine_name => $vocabulary_option) {
        // Ignore the None option.
        if (empty($vocab_machine_name)) {
          continue;
        }

        $term_options[$vocabulary_option] = array();
        $vocab_terms = $this->getTermOptions($vocab_machine_name);
        foreach ($vocab_terms as $tid => $term) {
          $term_options[$vocabulary_option][$tid] = $term;
        }
      }

      $form['term_any'] = array(
        '#title' => t('Taxonomy term (Any of)'),
        '#type' => 'select',
        '#multiple' => TRUE,
        '#options' => $term_options,
        '#description' => t('Only include content which matches any of these taxonomy terms.'),
        '#default_value' => $item['data']['term_any'],
      );

      $form['term_all'] = array(
        '#title' => t('Taxonomy term (All of)'),
        '#type' => 'select',
        '#multiple' => TRUE,
        '#options' => $term_options,
        '#description' => t('Only include content which matches all of these taxonomy terms.'),
        '#default_value' => $item['data']['term_all'],
      );

      $form['term_none'] = array(
        '#title' => t('Taxonomy term (None of)'),
        '#type' => 'select',
        '#multiple' => TRUE,
        '#options' => $term_options,
        '#description' => t('Only include content which matches none of these taxonomy terms.'),
        '#default_value' => $item['data']['term_none'],
      );
    }

    $form['order'] = array(
      '#type' => 'fieldset',
      '#title' => t('Order'),
      '#collapsible' => TRUE,
      'order_by' => array(
        '#type' => 'select',
        '#title' => t('Order by'),
        '#description' => t('Select how to sort this content.'),
        '#options' => array(
          'created' => t('Created'),
          'changed' => t('Modified'),
          'title' => t('Title'),
        ),
        '#required' => TRUE,
        '#default_value' => $item['data']['order']['order_by'],
      ),
      'order_method' => array(
        '#type' => 'select',
        '#title' => t('Order method'),
        '#description' => t('Select the sort direction.'),
        '#options' => array(
          'asc' => t('Ascending'),
          'desc' => t('Descending'),
        ),
        '#required' => TRUE,
        '#default_value' => $item['data']['order']['order_method'],
      ),
    );

    $form['age_limit'] = array(
      '#type' => 'textfield',
      '#title' => t('Age limit'),
      '#description' => t('Limit the list of content to those created in this number of days.'),
      '#default_value' => $item['data']['age_limit'],
      '#size' => 5,
    );

    $form['item_limit'] = array(
      '#type' => 'textfield',
      '#title' => t('Item limit'),
      '#description' => t('Limit the list of content to a certain number of content items.'),
      '#default_value' => $item['data']['item_limit'],
      '#size' => 5,
      '#required' => TRUE,
    );

    $form['item_pager'] = array(
      '#type' => 'textfield',
      '#title' => t('Items per page'),
      '#description' => t('Limit the number of content items on each page. Use 0 to show all items on 1 page.'),
      '#default_value' => $item['data']['item_pager'],
      '#size' => 5,
      '#required' => TRUE,
    );

    $form['display_mode'] = array(
      '#type' => 'select',
      '#title' => t('Display mode'),
      '#description' => t('Select the display mode for the individual content items.'),
      '#options' => $this->getDisplayOptions(),
      '#default_value' => $item['data']['display_mode'],
    );

    $form['list_display_mode'] = array(
      '#type' => 'select',
      '#title' => t('List display mode'),
      '#description' => t('Select the display mode for the overall list.'),
      '#options' => $this->getListDisplayOptions(),
      '#default_value' => !empty($item['data']['list_display_mode']) ? $item['data']['list_display_mode'] : 'default',
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function formValidate($element, &$form_state, $form) {
    // Use parent to validate required.
    parent::formValidate($element, $form_state, $form);

    // Only validate lister fields if they are EFQ Listers.
    if ($element['lister_type']['#value'] != 'lister_efq') {
      return;
    }

    // Validate age limit as an integer.
    $age_value = !empty($element['data']['age_limit']['#value']) ? $element['data']['age_limit']['#value'] : '';
    if (!empty($age_value) && (!is_numeric($age_value) || (float) intval($age_value) != floatval($age_value))) {
      form_error($element['data']['age_limit'], t('Age Limit must be a whole number.'));
    }

    // Validate item limit as an integer.
    $item_value = !empty($element['data']['item_limit']['#value']) ? $element['data']['item_limit']['#value'] : '';
    if (!empty($item_value) && (!is_numeric($item_value) || (float) intval($item_value) != floatval($item_value))) {
      form_error($element['data']['item_limit'], t('Item Limit must be a whole number.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render(array $item) {
    $output = array();
    $data = $item['data'];

    $query = new ListerEfqRangedPager();
    $query->entityCondition('entity_type', 'node')
      ->propertyCondition('status', 1);

    // Apply any entity ID conditions. These are done together to properly
    // combine them.
    // If this returns FALSE then there will never be any items so we can return
    // an empty array now.
    if (!$this->applyEntityIdConditions($query, $data)) {
      return array();
    }

    if (!empty($data['order']['order_by']) && !empty($data['order']['order_method'])) {
      $query->propertyOrderBy($data['order']['order_by'], $data['order']['order_method']);
    }

    // If the content type is set, add a condition.
    if (!empty($data['content_type'])) {
      $query->entityCondition('bundle', $data['content_type']);
    }

    // If we have a Group then filter by it.
    if (!empty($data['group'])) {
      $query->fieldCondition('og_group_ref', 'target_id', $data['group'], '=');
    }

    // If we have an age limit then add created condition to query.
    if (!empty($data['age_limit'])) {
      $query->propertyCondition('created', time() - (86400 * $data['age_limit']), '>');
    }

    // If we have an item limit then add range to query.
    if (!empty($data['item_limit'])) {
      $query->range(0, $data['item_limit']);
    }

    // Only add a pager if Items per Page is not empty.
    if (!empty($data['item_pager'])) {
      // If there is no upper item limit then just use the pager limit.
      if (empty($data['item_limit'])) {
        $query->pager($data['item_pager']);
      }
      else {
        // If item limit is smaller than the pager limit then an error is
        // thrown. Therefore, set the pager count to the lowest of the item
        // limit or the pager limit.
        $query->pager(min($data['item_limit'], $data['item_pager']));
      }
    }

    $result = $query->execute();

    // If there are no results then we can't process them.
    if (empty($result['node'])) {
      return $output;
    }

    $nodes = node_load_multiple(array_keys($result['node']));
    $display_mode = isset($data['display_mode']) ? $data['display_mode'] : 'teaser';

    foreach ($nodes as $node) {
      $output[] = node_view($node, $display_mode);
    }

    // Render a pager after the other content.
    $output['pager'] = array(
      '#theme' => 'pager',
    );

    $output['#attributes']['class'][] = 'lister--' . (isset($data['list_display_mode']) ? $data['list_display_mode'] : 'grid');

    return $output;
  }

  /**
   * Create Vocabulary options that can be used in all Taxonomy lists.
   *
   * @return array
   *   An array to be used in #options of all vocabularies with a None option.
   */
  protected function getVocabularyOptions() {
    $vocabulary_options = array(
      '' => t('None'),
    );
    foreach (taxonomy_vocabulary_get_names() as $vocabulary) {
      $vocabulary_options[$vocabulary->machine_name] = $vocabulary->name;
    }

    return $vocabulary_options;
  }

  /**
   * Create Term options based on Vocabulary ID.
   *
   * @param int $vocabulary_name
   *   The machine name of the Vocabulary the terms should be part of.
   *
   * @return array
   *   An array to be used in #options of a terms field.
   */
  protected function getTermOptions($vocabulary_name) {
    // Load vocabulary from name so that we can get the VID.
    $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_name);

    // Create term options array from passed in vocabulary.
    $term_options = array();
    foreach (taxonomy_get_tree($vocabulary->vid) as $term) {
      $term_options[$term->tid] = str_repeat('- ', $term->depth) . $term->name;
    }

    return $term_options;
  }

  /**
   * Create Display Mode options.
   *
   * @return array
   *   An array suitable for #options of all node view modes.
   */
  protected function getDisplayOptions() {
    $view_modes = array();

    $entity_info = entity_get_info('node');
    foreach ($entity_info['view modes'] as $machine_name => $view_mode) {
      $view_modes[$machine_name] = $view_mode['label'];
    }

    return $view_modes;
  }

  /**
   * Create List Display Mode options.
   *
   * @return array
   *   An array suitable for #options of available list display modes.
   */
  protected function getListDisplayOptions() {
    $view_modes = array();

    $view_modes = array(
      'default' => t('Default'),
      'grid' => t('Grid'),
    );

    return $view_modes;
  }

  /**
   * Check if an array is empty or only contains other empty values.
   *
   * @param array $item
   *   The array to check for emptiness.
   *
   * @return bool
   *   TRUE if the array is recursively empty, FALSE otherwise.
   */
  protected function recursiveEmpty(array $item) {
    $empty = TRUE;

    if (is_array($item) && count($item) > 0) {
      foreach ($item as $value) {
        $empty = $empty && $this->recursiveEmpty($value);
      }
    }
    else {
      $empty = empty($item);
    }

    return $empty;
  }

  /**
   * Applies the Terms Any, Terms All and Terms None Of filters to a query.
   *
   * If the lister field is on a node then also excludes that node from the
   * results to prevent nesting.
   *
   * @param EntityFieldQuery $query
   *   The query to apply conditions to.
   * @param array $data
   *   The Lister data to get value and settings from.
   *
   * @return bool
   *   Boolean for whether the query could return results. TRUE means results
   *   could be valid. FALSE means there can never be values for a query with
   *   this condition.
   */
  protected function applyEntityIdConditions(EntityFieldQuery &$query, array $data) {
    // If we have any Terms Any values then filter by them.
    $any_term_nodes = array();
    $any_term_selected = FALSE;
    if (isset($data['term_any'])) {
      foreach ($data['term_any'] as $tid) {
        $any_term_nodes = array_merge($any_term_nodes, taxonomy_select_nodes($tid, FALSE));
        $any_term_selected = TRUE;
      }
      // If any terms was filled in but no nodes meet that requirement then
      // we can stop processing and return FALSE.
      if ($any_term_selected && empty($any_term_nodes)) {
        return FALSE;
      }
    }

    // If we have any Terms All values then filter by them.
    // We can't use an empty array because Terms All requires
    // array_intersect which won't work with an empty array.
    $all_term_nodes = NULL;
    $all_term_selected = FALSE;
    if (isset($data['term_all'])) {
      foreach ($data['term_all'] as $tid) {
        $all_term_selected = TRUE;
        if (is_null($all_term_nodes)) {
          $all_term_nodes = taxonomy_select_nodes($tid, FALSE);
        }
        else {
          $all_term_nodes = array_intersect($all_term_nodes, taxonomy_select_nodes($tid, FALSE));
        }
      }
      // If all terms was filled in but no nodes meet that requirement then
      // we can stop processing and return FALSE.
      if ($all_term_selected && empty($all_term_nodes)) {
        return FALSE;
      }
    }

    if (!empty($all_term_nodes) && !empty($any_term_nodes)) {
      // Use intersect to combine these as if they were an AND.
      // These have to be combined because entityConditions with same operators
      // seem to get combined which would be the same as merging the array,
      // giving an OR type combination.
      $required_term_nodes = array_intersect($all_term_nodes, $any_term_nodes);
    }
    else {
      if (!empty($any_term_nodes)) {
        $required_term_nodes = $any_term_nodes;
      }
      elseif (!empty($all_term_nodes)) {
        $required_term_nodes = $all_term_nodes;
      }
    }

    $excluded_term_nodes = array();
    // If the current page is a node then exclude it from the lister to prevent
    // recursion.
    if ($menu_object = menu_get_object()) {
      $excluded_term_nodes[] = $menu_object->nid;
    }
    // If the lister field is on a node then always exclude that node from
    // results.
    if ($data['self']['entity_type'] == 'node') {
      $excluded_term_nodes[] = $data['self']['entity']->nid;
    }

    // If we have any Terms None of values then filter by them.
    if (isset($data['term_none'])) {
      foreach ($data['term_none'] as $tid) {
        $excluded_term_nodes = array_merge($excluded_term_nodes, taxonomy_select_nodes($tid, FALSE));
      }
      // If there are required & excluded node IDs then combine then simply
      // remove the excluded node IDs from the required array.
      if (!empty($required_term_nodes) && !empty($excluded_term_nodes)) {
        $required_term_nodes = array_diff($required_term_nodes, $excluded_term_nodes);
      }
    }

    // If any combination of Any & All were selected but no nodes meet the
    // requirements then return FALSE.
    if (($all_term_selected || $any_term_selected) && empty($required_term_nodes)) {
      return FALSE;
    }

    // If there are required and excluded nodes then the excluded have already
    // been removed from the required array.
    if (!empty($required_term_nodes)) {
      $query->entityCondition('entity_id', $required_term_nodes);
    }
    // If there are no required term nodes then make sure any nodes that should
    // be excluded are.
    elseif (!empty($excluded_term_nodes)) {
      $query->entityCondition('entity_id', $excluded_term_nodes, 'NOT IN');
    }

    // If we reached the end of the function then the query is valid so return
    // TRUE for the rest of the query to be constructed and executed.
    return TRUE;
  }

}
