<?php
/**
 * @file
 * Default template for Lister Items.
 */
?>
<div class="lister__item">
  <h2 class="lister__title"><?php print $title; ?></h2>
  <?php if (!empty($link)): ?>
    <span><?php print $link; ?></span>
  <?php endif; ?>
  <?php if (!empty($image)):
    print render($image);
  endif; ?>
  <div class="lister__description"><?php print $description ?></div>
</div>
