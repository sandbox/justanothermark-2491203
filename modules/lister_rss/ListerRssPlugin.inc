<?php
/**
 * @file
 * Contains ListerRssPlugin class.
 */

use Symfony\Component\CssSelector\CssSelector;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ListerRssPlugin.
 *
 * Provides a Lister Plugin based around RSS feeds for listing external
 * content.
 *
 * @see ListerPlugin
 * @see EntityFieldQuery
 */
class ListerRssPlugin extends ListerPlugin {
  /**
   * {@inheritdoc}
   */
  public function form(array $item, $delta, $element, $instance) {
    $form = array();

    $form['rss_feed'] = array(
      '#type' => 'textfield',
      '#title' => t('Feed URL'),
      '#default_value' => isset($item['data']['rss_feed']) ? $item['data']['rss_feed'] : '',
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function formValidate($element, &$form_state, $form) {
    // Use parent to validate required.
    parent::formValidate($element, $form_state, $form);

    // Only validate lister fields if they are RSS Listers.
    if ($element['lister_type']['#value'] != 'lister_rss') {
      return;
    }

    // Validate RSS Feed field as valid URL (absolute only).
    if (!empty($element['data']['rss_feed']['#value']) && !valid_url($element['data']['rss_feed']['#value'], TRUE)) {
      if (valid_url($element['data']['rss_feed']['#value'], FALSE)) {
        form_error($element['data']['rss_feed'], t('Invalid URL provided for RSS Feed. Ensure it has the protocol at the start (e.g. http:// or https://).'));
      }
      else {
        form_error($element['data']['rss_feed'], t('Invalid URL provided for RSS Feed.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render(array $item) {
    // Load the feed into a string.
    $feed = $this->urlGetContents($item['data']['rss_feed']);
    if (!$feed) {
      return array();
    }

    $crawler = new Crawler($feed);

    if (!$crawler) {
      return array();
    }

    CssSelector::disableHtmlExtension();
    $rss_items[] = $crawler->filter('item')->each(function (Crawler $node, $i) {
      $item = array();
      $item['#theme'] = 'lister_rss_item';
      $item['#title'] = filter_xss($node->filter('title')->text());
      $item['#link'] = $node->filter('link');
      $item['#link'] = count($item['#link']) ? check_url($item['#link']->text()) : NULL;
      $item['#description'] = filter_xss($node->filter('description')->text());
      $image = $node->filter('image');
      if (count($image)) {
        $item['#image'] = array(
          '#theme' => 'image',
          '#path' => check_url($node->filter('image > url')->text()),
          '#title' => check_plain($node->filter('image > title')->text()),
        );
      }
      return $item;
    });

    return $rss_items;
  }

  /**
   * Use CURL to fetch a file.
   *
   * @param string $url
   *   The URL to fetch.
   *
   * @return mixed
   *   The file contents as a string.
   */
  protected function urlGetContents($url) {
    $response = drupal_http_request($url);
    if ($response->code == 200) {
      return $response->data;
    }
    return '';
  }

}
