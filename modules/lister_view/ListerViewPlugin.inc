<?php
/**
 * @file
 * Contains ListerViewPlugin class.
 */

/**
 * Class ListerViewPlugin.
 *
 * Provides a Lister Plugin based around Views for user configured
 * content listing.
 *
 * @see ListerPlugin
 * @see EntityFieldQuery
 */
class ListerViewPlugin extends ListerPlugin {
  /**
   * {@inheritdoc}
   */
  public function form(array $item, $delta, $element, $instance) {
    $form = array();

    // Add defaults to the item data.
    $item['data'] += array(
      'view' => '',
    );

    $form['view'] = array(
      '#type' => 'select',
      '#title' => t('View'),
      '#options' => $this->viewsOptions($instance),
      '#empty_option' => ' - ' . t('Select') . ' - ',
      '#default_value' => $item['data']['view'],
      '#required' => TRUE,
      '#ajax' => array(
        'callback' => 'lister_view_options_ajax',
        'wrapper' => 'lister-view-filter-options',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );
    $form['options'] = array(
      '#type' => 'container',
      '#title' => t('View options'),
      '#prefix' => '<div id="lister-view-filter-options">',
      '#suffix' => '</div>',
    );

    $option_keys = array_keys($form['view']['#options']);
    $view_name = empty($item['data']['view']) ? reset($option_keys) : $item['data']['view'];
    $view = views_get_view($view_name);
    if (empty($view)) {
      return $form;
    }

    // If the View uses arguments then create inputs for each of them.
    if (!empty($view->display['default']->display_options['arguments'])) {
      $arguments = $view->display['default']->display_options['arguments'];

      foreach ($arguments as $argument) {
        $form['options'][$argument['id']] = lister_view_argument_form($view_name, $argument, isset($item['data']['options'][$argument['id']]) ? $item['data']['options'][$argument['id']] : NULL);
        if (!empty($form['options'][$argument['id']]['#multiple'])) {
          $form['options'][$argument['id']]['#default_value'] = explode('+', $form['options'][$argument['id']]['#default_value']);
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function formValidate($element, &$form_state, $form) {
    // Use parent to validate required.
    parent::formValidate($element, $form_state, $form);

    $field_name = $element['#field_name'];
    $delta = $element['#delta'];

    // Loop through the options and if any are set to #multiple then convert the
    // array to a string for Views.
    if (!empty($form_state['values'][$field_name][LANGUAGE_NONE][$delta]['lister']['data'])) {
      $data = &$form_state['values'][$field_name][LANGUAGE_NONE][$delta]['lister']['data'];

      if (empty($data['options'])) {
        return;
      }
      foreach ($data['options'] as $index => $value) {
        if (!empty($element['data']['options'][$index]['#multiple'])) {
          $data['options'][$index] = implode('+', $value);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render(array $item) {
    $output = array();
    $data = $item['data'];

    $view = views_get_view($data['view']);
    if (!($view)) {
      return $output;
    }
    // As there is no page display we need to override it.
    // See https://www.drupal.org/node/525592#comment-1833824 for more details.
    // We are overriding the URL rather than the path because path appends
    // contextual filters to the #action and we don't need or want that.
    $view->override_url = drupal_get_path_alias(current_path());

    // Ensure $data['options'] is an array before allowing other code to alter.
    $data['options'] = !empty($data['options']) && is_array($data['options']) ? $data['options'] : array();
    drupal_alter('lister_view_argument_render', $data['options']);
    // Filter any empty values so that the contextual filters don't apply.
    $data['options'] = array_filter($data['options']);
    $view->set_arguments($data['options']);
    $output[] = array('#markup' => $view->preview('default', array_values($data['options'])));

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function widgetForm(array $settings) {
    $form = array();

    $form['views'] = array(
      '#type' => 'select',
      '#title' => t('Views'),
      '#description' => t('Select the Views that can be referenced by this field.'),
      '#multiple' => TRUE,
      '#options' => views_get_views_as_options(TRUE, 'enabled'),
      '#default_value' => isset($settings['views']) ? $settings['views'] : array(),
    );

    return $form;
  }

  /**
   * Fetch a list of Views that can be used as options.
   *
   * @return array
   *   The options array of Views.
   */
  protected function viewsOptions($instance) {
    $options = array('' => t('None'));

    if (!empty($instance['widget']['settings']['lister_view']['views'])) {
      $views = $instance['widget']['settings']['lister_view']['views'];
      foreach ($views as $view_name) {
        $view = views_get_view($view_name);
        $options[$view_name] = $view->human_name;
      }
    }
    else {
      $options = views_get_views_as_options(TRUE, 'enabled');
    }

    return $options;
  }

}
