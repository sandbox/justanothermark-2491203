<?php
/**
 * @file
 * Contains ListerPlugin class.
 */

/**
 * Class ListerPlugin.
 *
 * Base class for all Lister Plugins. Provides default 'None' behaviour.
 */
class ListerPlugin {
  /**
   * Provides the widget form for this plugin.
   *
   * Fields in this form are automatically saved to the data attribute
   * of the item on submission. The item can be altered before it is saved by
   * overriding formSubmit().
   *
   * @param array $item
   *   The existing item being edited.
   *
   * @return array
   *   The form to be used in the field widget.
   *
   * @see formSubmit()
   * @see lister_field_presave()
   */
  public function form(array $item, $delta, $element, $instance) {
    return array();
  }

  /**
   * Validation handler for the widget form.
   *
   * @see form()
   */
  public function formValidate($element, &$form_state, $form) {
    // Intentionally left blank.
  }

  /**
   * Submit handler for the widget form.
   *
   * @param array $item
   *   An individual item to be saved.
   *
   * @see form()
   */
  public function formSubmit(array &$item) {
    // Intentionally left blank.
  }

  /**
   * Renders the widget for viewing.
   *
   * @param array $item
   *   An individual item to be rendered.
   *
   * @return array|string
   *   A renderable array or string for this item.
   */
  public function render(array $item) {
    return array();
  }

  /**
   * Provides the widget field settings form.
   *
   * @param array $settings
   *   Array of configuration options for the lister.
   *
   * @return array
   *   The form to be used in field settings.
   */
  public function widgetForm(array $settings) {
    return array();
  }

}
