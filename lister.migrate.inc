<?php
/**
 * @file
 * Migration code for Lister.
 */

/**
 * Implements hook_migrate_api().
 */
function lister_migrate_api() {
  $api = array(
    'api' => 2,
    'field handlers' => array(
      'ListerMigrateFieldHandler',
    ),
  );
  return $api;
}
