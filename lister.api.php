<?php
/**
 * @file
 * API documentation for Lister module.
 */

/**
 * Provide info about Lister plugins implemented in the module.
 */
function hook_lister_info() {
  $items = array();

  $items['rss'] = array(
    'name' => t('RSS'),
    'class' => 'ListerRssPlugin',
  );

  return $items;
}
