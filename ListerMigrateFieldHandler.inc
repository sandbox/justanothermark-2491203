<?php
/**
 * @file
 * Contains ListerMigrateFieldHandler.
 */

/**
 * Class ListerMigrateFieldHandler.
 */
class ListerMigrateFieldHandler extends MigrateFieldHandler {
  /**
   * Constructor for ListerMigrateFieldHandler.
   */
  public function __construct() {
    $this->registerTypes(array('lister'));
  }

  /**
   * Prepare values for migration.
   */
  public function prepare($entity, array $field_info, array $instance, array $values) {
    $arguments = array();

    if (isset($values['arguments'])) {
      $arguments = array_filter($values['arguments']);
      unset($values['arguments']);
    }
    $language = $this->getFieldLanguage($entity, $field_info, $arguments);

    // Setup the standard Field API array for saving.
    $delta = 0;
    $field_defaults = array(
      'lister_type' => 'lister_none',
      'data' => array(),
    );

    $return = array();
    foreach ($values as $value) {
      $value = is_null($value) ? array() : $value;
      $return[$language][$delta]['lister'] = array_merge($field_defaults, $value);
      $delta++;
    }

    return $return;
  }

  /**
   * Implements MigrateFieldHandler::arguments().
   */
  public static function arguments($lister_type = 'lister_none', $data = array()) {
    $arguments = array();
    $arguments['lister_type'] = $lister_type;
    $arguments['data'] = $data;
    return $arguments;
  }

  /**
   * Implementation of MigrateFieldHandler::fields().
   */
  public function fields($type, $instance, $migration = NULL) {
    $fields = array();

    $fields['lister_type'] = t('The Lister plugin used for this field.');
    $fields['data'] = t('Data array for the Lister plugin.');

    return $fields;
  }

}
