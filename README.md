Lister
=======

Introduction
------------

The Lister module provides a field type that allows for lists of content to be
created as a field.

Lister fields are plugin based to allow for the list to be generated in
multiple ways. Plugins included in submodules are EFQ (EntityFieldQuery), RSS,
Views and Menu Children. An abstract class ListerPlugin is provided which can
be extended for custom plugins.

Requirements
------------
The base Lister module requires only the core Field module. However, plugin
modules may require additional modules depending on their behaviour.

Recommended modules
-------------------
* Lister EFQ:
This included plugin module is the most likely use case for Lister. It allows
a user to create a list of content based on content type, Organic Groups
(https://drupal.org/project/og) or taxonomies.

Installation
------------
Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.

The Lister RSS module requires the Symfony DOMCrawler component. A
composer.json file is provided so that this can be installed with 
[Composer](https://getcomposer.org).

Configuration
-------------
The module is configured like any other field type. Each field provides a
description with any relevant information.

Maintainers
-----------
Current maintainers:
 * Mark Jones (justanothermark) - https://drupal.org/user/1369624
 * Matt Smith (splatio) - https://drupal.org/user/1012210

This project has been sponsored by:
 * CTI Digital - http://ctidigital.com
